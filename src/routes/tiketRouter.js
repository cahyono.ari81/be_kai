const express = require("express");
const tiketController = require("../controllers/tiketController");

const router = express.Router();

router.get("/:tiketId", tiketController.getById);
router.get("/", tiketController.getAll);

module.exports = router;