const express = require('express');
const Controller = require('../controllers/controller');
const authController = require("./authController");
const tiketRouter = require("./tiketRouter");
const tiketUserRouter = require("./tiketUserRouter");
const stasiunRouter = require("./stasiunRouter");
const { verifyToken } = require("../middleware/authJwt");

const router = express.Router();

router.get("/", Controller.helloWorld);
router.use("/", authController);
router.use("/tiket/", tiketRouter);
router.use("/tiket-user/", verifyToken ,tiketUserRouter);
router.use("/stasiun/", stasiunRouter);

module.exports = router;
