const express = require("express");
const tiketUserController = require("../controllers/tiketUserController");

const router = express.Router();

router.get("/", tiketUserController.getAll);
router.get("/:tiketUserId", tiketUserController.getById);
router.post("/", tiketUserController.store);
router.post("/nonaktif", tiketUserController.nonAktif);

module.exports = router;