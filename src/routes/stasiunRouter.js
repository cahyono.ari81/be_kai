const express = require("express");
const stasiunController = require("../controllers/stasiunController");

const router = express.Router();

router.get("/", stasiunController.getAll);

module.exports = router;
