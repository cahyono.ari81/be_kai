exports.response = (message = null, data = null, errors = null) => {
  return {
    message,
    data,
    errors,
  };
};
