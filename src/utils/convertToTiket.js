
exports.convertTiketList = (dataTiket) => {
  
  const data = dataTiket.map((data) => {
      const { KeretaApi, JadwalBerangkat, JadwalTiba } = data; 
      return {
        id_tiket: data.id,
        nama_kereta_api: KeretaApi.nama,
        kelas_kereta_api: KeretaApi.kelas,
        harga_tiket: data.harga,
        tanggal_berangkat: JadwalBerangkat.tgl_berangkat,
        waktu_berangkat: JadwalBerangkat.waktu_berangkat,
        tanggal_tiba: JadwalTiba.tgl_tiba,
        waktu_tiba: JadwalTiba.waktu_tiba,
        durasi: data.durasi,
        stasiun_berangkat: JadwalBerangkat.Stasiun.nama,
        stasiun_berangkat_inisial: JadwalBerangkat.Stasiun.inisial_stasiun,
        stasiun_tujuan: JadwalTiba.Stasiun.nama,
        stasiun_tujuan_inisial: JadwalTiba.Stasiun.inisial_stasiun,
      };
    });

    return data;
};

exports.convertTiket = (data) => {
      const { KeretaApi, JadwalBerangkat, JadwalTiba } = data; 
      return {
        id_tiket: data.id,
        nama_kereta_api: KeretaApi.nama,
        kelas_kereta_api: KeretaApi.kelas,
        harga_tiket: data.harga,
        tanggal_berangkat: JadwalBerangkat.tgl_berangkat,
        waktu_berangkat: JadwalBerangkat.waktu_berangkat,
        tanggal_tiba: JadwalTiba.tgl_tiba,
        waktu_tiba: JadwalTiba.waktu_tiba,
        durasi: data.durasi,
        stasiun_berangkat: JadwalBerangkat.Stasiun.nama,
        stasiun_berangkat_inisial: JadwalBerangkat.Stasiun.inisial_stasiun,
        stasiun_tujuan: JadwalTiba.Stasiun.nama,
        stasiun_tujuan_inisial: JadwalTiba.Stasiun.inisial_stasiun,
      };
};

exports.convertTiketUserList = (dataTiket) => {
      const data = dataTiket.map((data) => {
      const { KeretaApi, JadwalBerangkat, JadwalTiba } = data.Tiket 
      return {
        id_tiket_user: data.id,
        nama_kereta_api: KeretaApi.nama,
        kelas_kereta_api: KeretaApi.kelas,
        tanggal_berangkat: JadwalBerangkat.tgl_berangkat,
        stasiun_berangkat: JadwalBerangkat.Stasiun.nama,
        stasiun_berangkat_inisial: JadwalBerangkat.Stasiun.inisial_stasiun,
        stasiun_tujuan: JadwalTiba.Stasiun.nama,
        stasiun_tujuan_inisial: JadwalTiba.Stasiun.inisial_stasiun,
      };
    });

    return data;
};

exports.convertTiketUser = (dataTiket) => {
      const { KeretaApi, JadwalBerangkat, JadwalTiba } = dataTiket.Tiket 
      return {
        id_tiket_user: dataTiket.id,
        user_nomor_kk: dataTiket.User.nik,
        total_penumpang: dataTiket.total_penumpang,
        nama_kereta_api: KeretaApi.nama,
        kelas_kereta_api: KeretaApi.kelas,
        tanggal_berangkat: JadwalBerangkat.tgl_berangkat,
        stasiun_berangkat: JadwalBerangkat.Stasiun.nama,
        stasiun_berangkat_inisial: JadwalBerangkat.Stasiun.inisial_stasiun,
        stasiun_tujuan: JadwalTiba.Stasiun.nama,
        stasiun_tujuan_inisial: JadwalTiba.Stasiun.inisial_stasiun,
      };
};
