const {
  Tiket,
  JadwalBerangkat,
  JadwalTiba,
  Stasiun,
  KeretaApi,
} = require("../models");
const { response } = require("../utils/response");
const { convertTiketList, convertTiket } = require("../utils/convertToTiket");

exports.getAll = async (req, res) => {
  const { tgl_berangkat = null, stasiun_berangkat = null , stasiun_tujuan = null, kelas = null } = req.query;

  const dataTiket = await Tiket.findAll({
    include: [
      {
        model: JadwalBerangkat,
        where: { tgl_berangkat: tgl_berangkat },
        include: [
          {
            model: Stasiun,
            where: { nama: stasiun_berangkat },
          },
        ],
      },
      {
        model: JadwalTiba,
        include: [
          {
            model: Stasiun,
            where: { nama: stasiun_tujuan },
          },
        ],
      },
      {
        model: KeretaApi,
        where: { kelas: kelas },
      },
    ],
    required: true,
  });

  const resData = convertTiketList(dataTiket);

  res.status(200).json(response("Success", resData));
};

exports.getById = async (req, res) => {
  const { tiketId } = req.params;

  const dataTiket = await Tiket.findOne({
    include: [
      {
        model: JadwalBerangkat,
        include: [
          {
            model: Stasiun,
          },
        ],
      },
      {
        model: JadwalTiba,
        include: [
          {
            model: Stasiun,
          },
        ],
      },
      {
        model: KeretaApi,
      },
    ],
    where: {
      id: tiketId,
    },
    required: true,
  });

  const resData = convertTiket(dataTiket);

  res.status(200).json(response("Success", resData));
};
