const {
  Tiket,
  JadwalBerangkat,
  JadwalTiba,
  Stasiun,
  KeretaApi,
  TiketUser,
  User
} = require("../models");
const { response } = require("../utils/response");
const {
  convertTiketUserList,
  convertTiketUser,
 } = require("../utils/convertToTiket");

exports.getAll = async (req, res) => {

  const { status } = req.query;
  const { user_id } = req;

  const dataTiket = await TiketUser.findAll({
    include: [
      {
        model: User,
        where: {
          id: user_id,
        },
      },
      {
        model: Tiket,
        include: [
          {
            model: JadwalBerangkat,
            include: [
              {
                model: Stasiun,
              },
            ],
          },
          {
            model: JadwalTiba,
            include: [
              {
                model: Stasiun,
              },
            ],
          },
          {
            model: KeretaApi,
          },
        ],
      },
    ],
    where: {status : status},
    required: true,
  });

  const resData = convertTiketUserList(dataTiket)

  res.status(200).json(response("Success", resData));
};

exports.getById = async (req, res) => {

  const { tiketUserId } = req.params;
  const { user_id } = req;

  const dataTiket = await TiketUser.findOne({
    include: [
      {
        model: User,
        where: {
          id: user_id,
        },
      },
      {
        model: Tiket,
        include: [
          {
            model: JadwalBerangkat,
            include: [
              {
                model: Stasiun,
              },
            ],
          },
          {
            model: JadwalTiba,
            include: [
              {
                model: Stasiun,
              },
            ],
          },
          {
            model: KeretaApi,
          },
        ],
      },
    ],
    where: { id: tiketUserId},
    required: true,
  });

  const resData = convertTiketUser(dataTiket)

  res.status(200).json(response("Success", resData));
};

exports.store = async (req, res) => {
  const { tiket_id, total_harga, total_penumpang } = req.body;
  const { user_id } = req;

  const data = await TiketUser.create({
    user_id: user_id,
    tiket_id,
    total_harga,
    total_penumpang,
    status: "aktif",
  });
  res.status(200).json(response("Success", data));
};

exports.nonAktif = async (req, res) => {
  try {
    const { tiketUser_id } = req.body;
    const { user_id } = req;

    const data = await TiketUser.update(
      {
        status: "nonaktif",
      },
      { where: { id: tiketUser_id, user_id: user_id } }
    );
    res.status(200).json(response("Success", data));
  } catch (error) {
    res.status(400).json(response(error, []));
    
  }
};