const { Stasiun } = require("../models");
const { response } = require('../utils/response');

exports.getAll = async (req, res) => {
  // Get all data Stasiun
  const data = await Stasiun.findAll();
  res.status(200).json(response("Success", data));
};