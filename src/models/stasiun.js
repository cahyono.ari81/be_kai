'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Stasiun extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
        Stasiun.hasMany(models.JadwalBerangkat, {
          foreignKey: "id",
        });
        Stasiun.hasMany(models.JadwalTiba, {
          foreignKey: "id",
        });
    }
  }
  Stasiun.init(
    {
      nama: DataTypes.STRING,
      kota: DataTypes.STRING,
      inisial_stasiun: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "Stasiun",
      timestamps: false,
    }
  );
  return Stasiun;
};