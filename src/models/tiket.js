'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Tiket extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Tiket.belongsTo(models.KeretaApi, {
        foreignKey: "kereta_api_id",
      });
      Tiket.belongsTo(models.JadwalBerangkat, {
        foreignKey: "jadwal_berangkat_id",
      });
      Tiket.belongsTo(models.JadwalTiba, {
        foreignKey: "jadwal_tiba_id",
      });
      Tiket.hasMany(models.TiketUser, {
        foreignKey: "id",
      });
    }
  }
  Tiket.init(
    {
      durasi: DataTypes.STRING,
      harga: DataTypes.INTEGER,
      kereta_api_id: DataTypes.INTEGER,
      jadwal_berangkat_id: DataTypes.INTEGER,
      jadwal_tiba_id: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "Tiket",
      timestamps: false,
    }
  );
  return Tiket;
};