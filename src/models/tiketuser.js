'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class TiketUser extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
         TiketUser.belongsTo(models.User, {
           foreignKey: "user_id",
         });
          TiketUser.belongsTo(models.Tiket, {
            foreignKey: "tiket_id",
          });
    }
  }
  TiketUser.init(
    {
      status: DataTypes.STRING,
      total_penumpang: DataTypes.INTEGER,
      total_harga: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "TiketUser",
      timestamps: false,
    }
  );
  return TiketUser;
};