'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class JadwalBerangkat extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
        JadwalBerangkat.hasMany(models.Tiket, {
          foreignKey: "id",
        });
         JadwalBerangkat.belongsTo(models.Stasiun, {
           foreignKey: "stasiun_id",
         });
    }
  }
  JadwalBerangkat.init(
    {
      tgl_berangkat: DataTypes.STRING,
      waktu_berangkat: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "JadwalBerangkat",
      timestamps: false,
    }
  );
  return JadwalBerangkat;
};