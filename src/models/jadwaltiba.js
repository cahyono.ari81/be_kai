'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class JadwalTiba extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
       JadwalTiba.hasMany(models.Tiket, {
         foreignKey: "id",
       });
      JadwalTiba.belongsTo(models.Stasiun, {
        foreignKey: "stasiun_id",
      });
    }
  }
  JadwalTiba.init(
    {
      tgl_tiba: DataTypes.STRING,
      waktu_tiba: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "JadwalTiba",
      timestamps: false,
    }
  );
  return JadwalTiba;
};