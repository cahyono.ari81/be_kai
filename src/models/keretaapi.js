'use strict';
const {
  Model
} = require('sequelize');
const tiket = require('./tiket');
module.exports = (sequelize, DataTypes) => {
  class KeretaApi extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      KeretaApi.hasMany(models.Tiket, {
        foreignKey: "id",
      });
    }
  }
  KeretaApi.init(
    {
      nama: DataTypes.STRING,
      kelas: DataTypes.STRING,
      harga: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "KeretaApi",
      timestamps: false,
    }
  );
  return KeretaApi;
};