'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("Tikets", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      durasi: {
        type: Sequelize.STRING,
      },
      harga: {
        type: Sequelize.INTEGER,
      },
      kereta_api_id: {
        type: Sequelize.INTEGER,
        references: {
          model: "KeretaApis",
          key: "id",
        },
      },
      jadwal_berangkat_id: {
        type: Sequelize.INTEGER,
        references: {
          model: "JadwalBerangkats",
          key: "id",
        },
      },
      jadwal_tiba_id: {
        type: Sequelize.INTEGER,
        references: {
          model: "JadwalTibas",
          key: "id",
        },
      },
      // createdAt: {
      //   allowNull: false,
      //   type: Sequelize.DATE
      // },
      // updatedAt: {
      //   allowNull: false,
      //   type: Sequelize.DATE
      // }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Tikets');
  }
};