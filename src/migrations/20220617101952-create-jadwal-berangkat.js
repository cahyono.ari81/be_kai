'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("JadwalBerangkats", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      tgl_berangkat: {
        type: Sequelize.STRING,
      },
      waktu_berangkat: {
        type: Sequelize.STRING,
      },
      stasiun_id: {
        type: Sequelize.INTEGER,
        references: {
          model: "Stasiuns",
          key: "id",
        },
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('JadwalBerangkats');
  }
};