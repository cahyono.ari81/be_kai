'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("JadwalTibas", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      tgl_tiba: {
        type: Sequelize.STRING,
      },
      waktu_tiba: {
        type: Sequelize.STRING,
      },
      stasiun_id: {
        type: Sequelize.INTEGER,
        references: {
          model: "Stasiuns",
          key: "id",
        },
      },
      // createdAt: {
      //   allowNull: false,
      //   type: Sequelize.DATE
      // },
      // updatedAt: {
      //   allowNull: false,
      //   type: Sequelize.DATE
      // }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('JadwalTibas');
  }
};