'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

     const dataStasiun = [
       {
         nama: "surabaya pasar turi",
         kota: "kota surabaya",
         inisial_stasiun: "sbi",
       },
       {
         nama: "pasar senen",
         kota: "kota jakarta",
         inisial_stasiun: "pse",
       },
       {
         nama: "gambir",
         kota: "kota jakarta",
         inisial_stasiun: "gmr",
       },
       {
         nama: "semarang poncol",
         kota: "kota semarang",
         inisial_stasiun: "smc",
       },
       {
         nama: "semarang tawang",
         kota: "kota semarang",
         inisial_stasiun: "smt",
       },
     ];
       await queryInterface.bulkInsert("Stasiuns", dataStasiun);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("Stasiuns", null, {});
  }
};
