'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

      const data = [
        {
          tgl_tiba: "19-06-2022",
          waktu_tiba: "02:45",
          stasiun_id: 1,
        },
        {
          tgl_tiba: "19-06-2022",
          waktu_tiba: "01:25",
          stasiun_id: 1,
        },
        {
          tgl_tiba: "19-06-2022",
          waktu_tiba: "03:39",
          stasiun_id: 1,
        },
      ];

     await queryInterface.bulkInsert("JadwalTibas", data);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("JadwalTibas", null, {});
  }
};
