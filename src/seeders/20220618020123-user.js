'use strict';
const bcrypt = require("bcrypt");

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     * 
    */

    const salt = await bcrypt.genSalt();
    const hashPassword = await bcrypt.hash("test", salt);

    const data = [
      {
        username: "Ari Cahyono",
        email: "test@gmail.com",
        nik: "23138131319209319",
        password: hashPassword,
      },
    ];

    await queryInterface.bulkInsert("Users", data);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
   await queryInterface.bulkDelete("Users", null, {});
  }
};
