'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

      const dataKeretaApi = [
        {
          nama: "dharmawangsa",
          kelas: "ekonomi",
          harga: 19090,
        },
        {
          nama: "kertajaya",
          kelas: "ekonomi",
          harga: 23181,
        },
        {
          nama: "jayabaya",
          kelas: "ekonomi",
          harga: 28636,
        },
      ];

      await queryInterface.bulkInsert("KeretaApis", dataKeretaApi);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete("keretaApis", null, {});
  }
};
