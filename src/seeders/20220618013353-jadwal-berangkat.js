'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

      const data = [
        {
          tgl_berangkat: "18-06-2022",
          waktu_berangkat: "15:50",
          stasiun_id: 2,
        },
        {
          tgl_berangkat: "18-06-2022",
          waktu_berangkat: "14:10",
          stasiun_id: 2,
        },
        {
          tgl_berangkat: "18-06-2022",
          waktu_berangkat: "16:45",
          stasiun_id: 2,
        },
      ];

     await queryInterface.bulkInsert("JadwalBerangkats", data);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
   await queryInterface.bulkDelete("JadwalBerangkats", null, {});
  }
};
