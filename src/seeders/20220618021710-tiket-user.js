'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

     const data = [
       {
         status: "aktif",
         total_penumpang: 4,
         total_harga: 220000,
         user_id: 1,
         tiket_id: 1,
       },
       {
         status: "aktif",
         total_penumpang: 4,
         total_harga: 220000,
         user_id: 1,
         tiket_id: 2,
       },
     ];

     await queryInterface.bulkInsert("TiketUsers", data);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("TiketUsers", null, {});
  }
};
