'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    const data = [
      {
        durasi: "10J 40m(+1 hari)",
        harga: 210000,
        kereta_api_id: 1,
        jadwal_berangkat_id: 1,
        jadwal_tiba_id: 1,
      },
      {
        durasi: "11J 15m(+1 hari)",
        harga: 225000,
        kereta_api_id: 2,
        jadwal_berangkat_id: 2,
        jadwal_tiba_id: 2,
      },
      {
        durasi: "11J 15m(+1 hari)",
        harga: 315000,
        kereta_api_id: 3,
        jadwal_berangkat_id: 3,
        jadwal_tiba_id: 3,
      },
      // {
      //   durasi: "10J 55m(+1 hari)",
      //   harga: 550000,
      //   kereta_api_id: 1,
      //   jadwal_berangkat_id: 1,
      //   jadwal_tiba_id: 1,
      // },
      // {
      //   durasi: "10J 55m(+1 hari)",
      //   harga: 550000,
      //   kereta_api_id: 1,
      //   jadwal_berangkat_id: 1,
      //   jadwal_tiba_id: 1,
      // },
    ];

    await queryInterface.bulkInsert("Tikets", data);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */

     await queryInterface.bulkDelete("Tikets", null, {});
  }
};
